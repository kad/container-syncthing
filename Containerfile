FROM docker.io/library/alpine:latest

ARG SYNCTHING_VERSION=1.28.0

ENV SYNCTHING_HTTP_PORT 8384
ENV LANG en_US.UTF-8
ENV SYNCTHING_VERSION=${SYNCTHING_VERSION}

RUN apk --no-progress update && \
    apk --no-progress upgrade && \
    apk --no-progress add curl tar dumb-init && \
    mkdir -pv /syncthing && \
    chmod -v 0777 /syncthing && \
    curl --silent \
        --location \
        --output /tmp/syncthing-linux-amd64-v${SYNCTHING_VERSION}.tar.gz \
        https://github.com/syncthing/syncthing/releases/download/v$SYNCTHING_VERSION/syncthing-linux-amd64-v${SYNCTHING_VERSION}.tar.gz && \
    tar -C /syncthing -xvzf /tmp/syncthing-linux-amd64-v${SYNCTHING_VERSION}.tar.gz && \
    mv -v /syncthing/syncthing-linux-amd64-v${SYNCTHING_VERSION}/syncthing /usr/local/bin/syncthing && \
    chmod -v 0755 /usr/local/bin/syncthing

EXPOSE $SYNCTHING_HTTP_PORT

COPY start.sh /start.sh

ENTRYPOINT ["/usr/bin/dumb-init"]
CMD ["/start.sh"]
